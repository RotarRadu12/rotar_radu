package edu1.utcn1.str1.colocviu2;

import java.util.concurrent.Semaphore;

public class Thread1 extends Thread {
    private Semaphore semaphore;
    private String pact1, pact2, pact3;
    private int p;

    public Thread1(Semaphore semaphore, String pact1, String pact2, String pact3, int p) {
        this.semaphore = semaphore;
        this.pact1 = pact1;
        this.pact2 = pact2;
        this.pact3 = pact3;
        this.p = p;
    }

    @Override
    public void run() {
        System.out.println(this.getName() + " - STATE 1");
        System.out.println("Non-timed activity: " + pact1);
        try {
            semaphore.acquire();
        } catch (Exception ignored) {
        }
        System.out.println(this.getName() + " - STATE 2");
        int k = p;
        semaphore.release();
        System.out.println(this.getName() + " - STATE 3");
        System.out.println("Non-timed activity: " + pact3);
    }
}
