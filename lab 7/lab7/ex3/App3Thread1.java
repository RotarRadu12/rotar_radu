package ex3;

import util.ActivityUtils;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Lock;

public class App3Thread1 extends Thread{
    private Lock lockul;
    private CountDownLatch countDownLatch, countDownLatch2;
    private int theradtime, minim, maxim;

    public App3Thread1(Lock lockul, CountDownLatch countDownLatch, CountDownLatch countDownLatch2, int theradtime, int minim, int maxim) {
        this.lockul = lockul;
        this.countDownLatch = countDownLatch;
        this.countDownLatch2 = countDownLatch2;
        this.theradtime = theradtime;
        this.minim = minim;
        this.maxim = maxim;
    }

    @Override
    public void run() {
        ActivityUtils.activity("A1");
        lockul.lock();
        ActivityUtils.timedTransition(theradtime);
        lockul.unlock();
        ActivityUtils.timedActivity(minim, maxim, "A2");

        countDownLatch.countDown();
        countDownLatch2.countDown();

        ActivityUtils.activity("A3");
    }
}
