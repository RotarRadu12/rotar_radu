package ex2;

import util.ActivityUtils;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Lock;

public class App2Thread1 extends Thread {
    private Lock lock;
    private Semaphore semaphore;
    private CyclicBarrier cyclicBarrier;
    private int min;
    private int max;
    private int t;

    public App2Thread1(Lock lock, Semaphore semaphore, CyclicBarrier cyclicBarrier, int min, int max, int t) {
        this.lock = lock;
        this.semaphore = semaphore;
        this.cyclicBarrier = cyclicBarrier;
        this.min = min;
        this.max = max;
        this.t = t;
    }

    @Override
    public void run() {
        while (true) {
            ActivityUtils.activity("A1");

            lock.lock();
            semaphore.release();

            ActivityUtils.timedActivity(min, max, "A2");
            lock.unlock();
            ActivityUtils.timedTransition(t);

            ActivityUtils.activity("A3");

            try {
                cyclicBarrier.await();
            } catch (Exception ignored) {
            }
        }
    }
}
