package ex4;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Main {
    public static void main(String[] args) {
        Lock lockul = new ReentrantLock();
        CountDownLatch countDownLatch = new CountDownLatch(1);

        new App4Thread(lockul, countDownLatch, 4, 7, 3).start();
        new App4Thread(lockul, countDownLatch, 5, 7, 6).start();
        new App4Thread(lockul, countDownLatch, 3, 6, 5).start();
    }
}
