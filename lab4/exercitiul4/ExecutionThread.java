package exercitiul4;

public class ExecutionThread extends Thread {
    Integer mon;
    int sleep_min, sleep_max, act_min, act_max;

    public ExecutionThread(Integer mon, int sleep_min, int sleep_max, int act_min, int act_max) {
        this.mon = mon;
        this.sleep_min = sleep_min;
        this.sleep_max = sleep_max;
        this.act_min = act_min;
        this.act_max = act_max;
    }

    public void run() {
        System.out.println(this.getName() + " - STAREA 1");
        try {
            Thread.sleep(Math.round(Math.random() * (sleep_max
                    - sleep_min)+ sleep_min) * 500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(this.getName() + " - STAREA 2");
        synchronized (mon) {
            System.out.println(this.getName() + " - STAREA 3");
            int k = (int) Math.round(Math.random()*(act_max
                    - act_min) + act_min);
            for (int i = 0; i < k * 100000; i++) {
                i++; i--;
            }
        }
        System.out.println(this.getName() + " - STAREA 4");
    }
}