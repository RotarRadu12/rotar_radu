package ex3;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Main {
    public static void main(String[] args) {
        Lock lockul = new ReentrantLock();
        CountDownLatch countDownLatch = new CountDownLatch(1);
        CountDownLatch countDownLatch2 = new CountDownLatch(1);
        new App3Thread1(lockul, countDownLatch, countDownLatch2, 7, 2, 3).start();
        new App3Thread2(lockul, countDownLatch, 5, 3, 5).start();
        new App3Thread2(lockul, countDownLatch, 5, 4, 6).start();
    }
}
