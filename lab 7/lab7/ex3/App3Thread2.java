package ex3;

import util.ActivityUtils;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Lock;

public class App3Thread2 extends Thread{
    private Lock lockul;
    private CountDownLatch countDownLatch;
    private int threadtime, minim, maxim;

    public App3Thread2(Lock lockul, CountDownLatch countDownLatch, int threadtime, int minim, int maxim) {
        this.lockul = lockul;
        this.countDownLatch = countDownLatch;
        this.threadtime = threadtime;
        this.minim = minim;
        this.maxim = maxim;
    }

    @Override
    public void run() {
        ActivityUtils.activity("A1");
        lockul.lock();
        ActivityUtils.timedTransition(threadtime);
        lockul.unlock();
        ActivityUtils.timedActivity(minim, maxim, "A2");
        try {
            countDownLatch.await();
        } catch (InterruptedException ignored) {

        }

        ActivityUtils.activity("A3");
    }
}
