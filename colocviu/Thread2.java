package edu1.utcn1.str1.colocviu2;

import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Lock;

public class Thread2 extends Thread {
    private Semaphore semaphore;
    private String pact1, pact2, pact3, pact4;
    private int p, t;

    public Thread2(Semaphore semaphore, String pact1, String pact2, String pact3, String pact4, int p, int t) {
        this.semaphore = semaphore;
        this.pact1 = pact1;
        this.pact2 = pact2;
        this.pact3 = pact3;
        this.pact4 = pact4;
        this.p = p;
        this.t = t;
    }

    @Override
    public void run() {
        System.out.println(this.getName() + " - STATE 1");
        System.out.println("Non-timed activity: " + pact1);
        try {
            semaphore.acquire();
        } catch (Exception ignored) {
        }
        System.out.println(this.getName() + " - STATE 2");
        int k = p;
        semaphore.release();
        System.out.println(this.getName() + " - STATE 3");
        System.out.println("Non-timed activity: " + pact3);
        try {
            Thread.sleep(t*1000);
        } catch (Exception ignored) {
        }
        System.out.println(this.getName() + " - STATE 4");
        System.out.println("Non-timed activity: " + pact4);
    }
}
