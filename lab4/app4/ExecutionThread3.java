package app4;

public class ExecutionThread3 extends Thread {
    Object mon;
    int act_min, act_max;

    public ExecutionThread3(Object mon, int act_min, int act_max) {
        this.mon = mon;
        this.act_min = act_min;
        this.act_max = act_max;
    }

    @Override
    public void run() {
        System.out.println(this.getName() + " - STAREA 1");
        try{
            synchronized (mon) {
                wait();
            }
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        System.out.println(this.getName() + " - STAREA 2");
        int k = (int) Math.round(Math.random() * (act_max - act_min) + act_min);
        for(int i=0; i<k*100000; i++){
            i++; i--;
        }
        System.out.println(this.getName() + " - STAREA 3");
    }
}
