package app4;

public class Main {
    public static void main(String[] args) {
        final Object mon = new Object();
        final Object mon2 = new Object();
        ExecutionThread thread = new ExecutionThread(7, 2, 3);
        ExecutionThread3 thread3 = new ExecutionThread3(mon2, 4, 6);
        ExecutionThread2 thread2 = new ExecutionThread2(mon, thread3, 3, 5);
        thread.start();
        thread2.start();
        thread3.start();
    }
}
