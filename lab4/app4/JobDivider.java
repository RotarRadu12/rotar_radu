package app4;

public class JobDivider extends Thread {
    ExecutionThread thread;

    public JobDivider(ExecutionThread t) {
        this.t = t;
    }

    synchronized void aJob(ExecutionThread t){
        System.out.println(t.getName() + " said: I did a job.");
        notify();
    }
    synchronized void otherJob(ExecutionThread t){
        System.out.println(t.getName() + " said: I did the other job.");
        notify();
    }
}
