package ex1;

import util.ActivityUtils;

import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Lock;

public class App1Thread2 extends Thread {
    private Semaphore semaphore;
    private Lock lockul1;
    private CyclicBarrier cyclicBarrier;
    private int minim1;
    private int maxim1;
    private int minim2;
    private int maxim2;
    private int thread;

    public App1Thread2(Semaphore semaphore, Lock lockul1, CyclicBarrier cyclicBarrier, int minim1, int maxim1, int minim2, int maxim2, int thread) {
        this.semaphore = semaphore;
        this.lockul1 = lockul1;
        this.cyclicBarrier = cyclicBarrier;
        this.minim1 = minim1;
        this.maxim1 = maxim1;
        this.minim2 = minim2;
        this.maxim2 = maxim2;
        this.thread = thread;
    }

    @Override
    public void run() {
        while(true) {
            ActivityUtils.timedActivity(minim1, maxim1, "A1");

            ActivityUtils.timedActivity(minim2, maxim2, "A2");
            ActivityUtils.activity("A3");

            ActivityUtils.timedTransition(thread);

            try {
                semaphore.acquire();
            } catch (InterruptedException ignored) {

            }
            ActivityUtils.activity("A4");

            try {
                cyclicBarrier.await();
            } catch (Exception e) {
            }
        }
    }
}
