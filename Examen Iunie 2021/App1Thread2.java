package ex1;



import java.util.HashSet;

import java.util.concurrent.Semaphore;

public class App1Thread2 extends Thread {
    private Semaphore sem;
    private HashSet<Integer> tempSet;


    public App1Thread2(HashSet<Integer> tempSet, Semaphore sem, ) {
        this.tempSet = tempSet;
        this.sem = sem;

    }

    @Override
    public void run() {
        while(true) {

            System.out.println("Thread 2 - started. Waiting for number generation");
            try {
                sem.acquire();
            } catch (InterruptedException ignored) {
            }

            int m = tempSet.stream().findFirst();
            int n,sum;
            sum =0;
            while(m > 0)
            {
                n = m % 10;
                sum = sum + n;
                m = m / 10;
            }
            System.out.println("Thread 2 - message:"+sum);
            System.out.println("Thread 2 - done");

        }
    }


}
