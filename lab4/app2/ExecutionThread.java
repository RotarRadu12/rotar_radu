package app2;

public class ExecutionThread extends Thread{
    Integer mon, mon2;
    int act_min1, act_max1, act_min2, act_max2, sleep;

    public ExecutionThread(Integer mon, Integer mon2, int act_min1, int act_max1, int act_min2, int act_max2, int sleep) {
        this.mon = mon;
        this.mon2 = mon2;
        this.act_min1 = act_min1;
        this.act_max1 = act_max1;
        this.act_min2 = act_min2;
        this.act_max2 = act_max2;
        this.sleep = sleep;
    }

    public void run(){
        System.out.println(this.getName() + " - STAREA 1");
        int k = (int)Math.round(Math.random() * (act_max1 - act_min1) + act_min1);
        for(int i=0; i<k*100000; i++) {
            i++;
            i--;
        }
        synchronized (mon) {
            System.out.println(this.getName() + " - STAREA 2");
            k = (int) Math.round(Math.random() * (act_max2 - act_min2) + act_min2);
            for (int i = 0; i < k * 100000; i++) {
                i++;
                i--;
            }
            synchronized (mon2) {
                System.out.println(this.getName() + " - STAREA 3");
                try {
                    Thread.sleep(Math.round((Math.random() * sleep) * 500));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println(this.getName() + " - STAREA 4");
    }
}
