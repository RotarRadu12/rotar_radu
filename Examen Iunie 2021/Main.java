package ex1;


import java.util.concurrent.Semaphore;
import java.util.HashSet;


public class Main {
    public static void main(String[] args) {
        HashSet<Integer> tempSet = new HashSet<Integer>();

        Semaphore sem = new Semaphore(0); // replace this with a Semaphore


        new ex1.App1Thread1(tempSet, semaphore).start();
        new ex1.App1Thread2(tempSet, semaphore).start();
    }
}
