package edu1.utcn1.str1.colocviu2;

import java.util.concurrent.Semaphore;

public class Main {
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(2);
        new Thread1(semaphore, "p2", "p5", "p9", 5).start();
        new Thread1(semaphore, "p3", "p6", "p10", 3).start();
        new Thread2(semaphore, "p4", "p7", "p11", "p12", 4, 3).start();
    }
}
