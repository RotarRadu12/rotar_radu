package app4;

public class ExecutionThread extends Thread {
    int sleep, act_min, act_max;
    JobDivider jd;

    public ExecutionThread(int sleep, int act_min, int act_max) {
        this.sleep = sleep;
        this.act_min = act_min;
        this.act_max = act_max;
    }

    @Override
    public void run() {
        System.out.println(this.getName() + " - STATE 1");
        try{
            Thread.sleep(Math.round(Math.random() * sleep) * 500);
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        System.out.println(this.getName() + " - STATE 2");
        int k = (int) Math.round(Math.random() * (act_max - act_min) + act_min);
        for(int i=0; i<k*100000; i++){
            i++; i--;
        }
        jd.aJob(this);
        jd.otherJob(this);
        System.out.println(this.getName() + " - STATE 3");
    }
}
