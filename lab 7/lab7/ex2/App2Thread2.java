package ex2;

import util.ActivityUtils;

import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Lock;

public class App2Thread2 extends Thread {
    private Semaphore semaphore;
    private Lock lock;
    private CyclicBarrier cyclicBarrier;
    private int min;
    private int max;
    private int t;

    public App2Thread2(Semaphore semaphore, Lock lock, CyclicBarrier cyclicBarrier, int min, int max, int t) {
        this.semaphore = semaphore;
        this.lock = lock;
        this.cyclicBarrier = cyclicBarrier;
        this.min = min;
        this.max = max;
        this.t = t;
    }

    @Override
    public void run() {
        while(true) {
            ActivityUtils.activity("A1");

            ActivityUtils.timedActivity(min, max, "A2");

            ActivityUtils.timedTransition(t);
            try {
                semaphore.acquire();
            } catch (InterruptedException ignored) {

            }
            ActivityUtils.activity("A3");


            try {
                cyclicBarrier.await();
            } catch (Exception ignored) {
            }
        }
    }
}
