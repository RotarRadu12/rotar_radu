package app4;

public class ExecutionThread2 extends Thread{
    Object monitor;
    Thread t;
    int activity_min, activity_max;

    public ExecutionThread2(Object monitor, Thread t, int activity_min, int activity_max) {
        this.monitor = monitor;
        this.t = t;
        this.activity_min = activity_min;
        this.activity_max = activity_max;
    }

    @Override
    public void run() {
        System.out.println(this.getName() + " - STATE 1");
        try{
            synchronized (monitor) {
                wait();
            }
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        System.out.println(this.getName() + " - STATE 2");
        int k = (int) Math.round(Math.random() * (activity_max - activity_min) + activity_min);
        for(int i=0; i<k*100000; i++){
            i++; i--;
        }

        System.out.println(this.getName() + " - STATE 3");
    }
}
