package util;

public class ActivityUtils {
    public static void timedActivity(int minim, int maxim, String activityName) {
        System.out.println(String.format("Incepe activitatea %s", activityName));

        int m = (int) Math.round(Math.random() * (maxim - minim) + minim);

        for (int i = 0; i < m * 100000; i++) {
            i++;
            i--;
        }
    }

    public static void activity(String activityName) {
        System.out.println(String.format("Incepe activitatea %s", activityName));
    }

    public static void timedTransition(int threadtime) {
        try {
            Thread.sleep(threadtime * 1000);
        } catch (InterruptedException ignored) {
        }
    }
}
