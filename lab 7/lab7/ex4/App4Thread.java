package ex4;

import util.ActivityUtils;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Lock;

public class App4Thread extends Thread {
    private Lock lockul;
    private CountDownLatch countDownLatch;
    private int minim;
    private int maxim;
    private int threadtime;

    public App4Thread(Lock lockul, CountDownLatch countDownLatch, int minim, int maxim, int threadtime) {
        this.lockul = lockul;
        this.countDownLatch = countDownLatch;
        this.minim = minim;
        this.maxim = maxim;
        this.threadtime = threadtime;
    }

    public void run() {
        while (true) {
            ActivityUtils.activity("A1");

            ActivityUtils.timedActivity(minim, maxim, "A2");

            countDownLatch.countDown();
            ActivityUtils.activity("A3");
            try {
                countDownLatch.await();
            } catch (InterruptedException ignored) {

            }
            lockul.lock();
            ActivityUtils.timedTransition(threadtime);
            lockul.unlock();
            ActivityUtils.activity("A4");
        }
    }
}
