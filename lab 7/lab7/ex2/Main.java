package ex2;

import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Main {
    public static void main(String[] args) {
        Lock lock1 = new ReentrantLock();
        Lock lock2 = new ReentrantLock();
        Semaphore semaphore = new Semaphore(0);
        CyclicBarrier cyclicBarrier = new CyclicBarrier(3);

        new App2Thread1(lock1, semaphore, cyclicBarrier, 2, 4, 4).start();
        new App2Thread1(lock2, semaphore, cyclicBarrier, 3, 6, 3).start();
        new App2Thread2(semaphore, lock1, cyclicBarrier, 2, 5, 5).start();
        new App2Thread2(semaphore, lock2, cyclicBarrier, 3, 6, 3).start();
    }
}
