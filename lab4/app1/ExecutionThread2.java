package app1;

public class ExecutionThread2 extends Thread {
    Integer mon, mon2;
    int act_min, act_max, sleep;

    public ExecutionThread2(Integer mon, Integer mon2, int act_min, int act_max, int sleep) {
        this.mon = mon;
        this.mon2 = mon2;
        this.act_min = act_min;
        this.act_max = act_max;
        this.sleep = sleep;
    }

    @Override
    public void run() {
        System.out.println(this.getName() + " - STATE 1");
        synchronized (mon){
            synchronized (mon2){
                System.out.println(this.getName() + " - STATE 2");
                int k=(int) Math.round(Math.random() * (act_max - act_min) + act_min);
                for(int i=0; i<k*100000; i++){
                    i++; i--;
                }
                System.out.println(this.getName() + " - STATE 3");
                try{
                    Thread.sleep(Math.round(Math.random() * sleep) * 500);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }
            }
        }
        System.out.println(this.getName() + " - STATE 4");
    }
}
