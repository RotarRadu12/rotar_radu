package ex1;

import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Main {
    public static void main(String[] args) {
        Lock lockul1 = new ReentrantLock(); // replace this with a ReentrantLock
        Semaphore lockul2 = new Semaphore(0); // replace this with a Semaphore
        CyclicBarrier cyclicBarrier = new CyclicBarrier(2); // replace this with a CyclicBarrier

        new ex1.App1Thread1(lockul1, lockul2, cyclicBarrier, 2, 4, 4, 6, 4).start();
        new ex1.App1Thread2(lockul2, lockul1, cyclicBarrier, 3, 5, 5, 7, 5).start();
    }
}
