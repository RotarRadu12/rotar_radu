package ex1;



import java.util.HashSet;

import java.util.concurrent.Semaphore;

public class App1Thread1 extends Thread {
    private HashSet<Integer> tempSet;
    private Semaphore semaphore;

    public App1Thread1(HashSet<Integer> tempSet, Semaphore semaphore) {
        this.tempSet = tempSet;
        this.semaphore = semaphore;

    }

    @Override
    public void run() {



        System.out.println("Thread 1 - started");
        int k = (int) Math.random();
        tempSet.add(k);
        semaphore.release();

        System.out.println("Thread 1 - done");



    }
}
